package com.stock.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MouvementDuStock implements Serializable {

	public static final  int ENTRE = 1 ; 
	public static final  int SORTIE = 2 ; 
	
	@Id
	@GeneratedValue
	private Long mouvementDuStockId;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date mouvementDate ; 
	
	private BigDecimal quantit�; 
	
	private int mouvementDuStockType; 
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article ;

	public Long getMouvementDuStockId() {
		return mouvementDuStockId;
	}

	public void setMouvementDuStockId(Long mouvementDuStockId) {
		this.mouvementDuStockId = mouvementDuStockId;
	}

	public Date getMouvementDate() {
		return mouvementDate;
	}

	public void setMouvementDate(Date mouvementDate) {
		this.mouvementDate = mouvementDate;
	}

	public BigDecimal getQuantit�() {
		return quantit�;
	}

	public void setQuantit�(BigDecimal quantit�) {
		this.quantit� = quantit�;
	}

	public int getMouvementDuStockType() {
		return mouvementDuStockType;
	}

	public void setMouvementDuStockType(int mouvementDuStockType) {
		this.mouvementDuStockType = mouvementDuStockType;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Long getId() {
		return mouvementDuStockId;
	}

	public void setId(Long id) {
		this.mouvementDuStockId = id;
	}

}
