package com.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommandeClient implements Serializable {

	@Id
	@GeneratedValue
	private Long ligneCommandeClientId;

	
	@ManyToOne
	@JoinColumn(name ="idArticle")
	private Article article;

	@ManyToOne
	@JoinColumn(name ="commandeClientId")
	private CommandeClient commandeClient;

	public Long getId() {
		return ligneCommandeClientId;
	}

	public void setId(Long id) {
		this.ligneCommandeClientId = id;
	}

	public Long getLigneCommandeClientId() {
		return ligneCommandeClientId;
	}

	public void setLigneCommandeClientId(Long ligneCommandeClientId) {
		this.ligneCommandeClientId = ligneCommandeClientId;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeClient getCommandeClient() {
		return commandeClient;
	}

	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	}

}
