package com.stock.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Vente implements Serializable {

	@Id
	@GeneratedValue
	private Long venteId;
	
	private String code ;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateVente; 
	
	@OneToMany(mappedBy = "vente")
	private List<LigneDeVente> ligneVentes; 


	public Long getVenteId() {
		return venteId;
	}

	public void setVenteId(Long venteId) {
		this.venteId = venteId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateVente() {
		return dateVente;
	}

	public void setDateVente(Date dateVente) {
		this.dateVente = dateVente;
	}

	public List<LigneDeVente> getLigneVentes() {
		return ligneVentes;
	}

	public void setLigneVentes(List<LigneDeVente> ligneVentes) {
		this.ligneVentes = ligneVentes;
	}

	public Long getId() {
		return venteId;
	}

	public void setId(Long id) {
		this.venteId = id;
	}

}
