package com.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneDeVente implements Serializable {

	@Id
	@GeneratedValue
	private Long ligneDeVenteId;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article ;
	
	@ManyToOne
	@JoinColumn(name = "vente")
	private Vente vente ;

	public Long getLigneDeVenteId() {
		return ligneDeVenteId;
	}

	public void setLigneDeVenteId(Long ligneDeVenteId) {
		this.ligneDeVenteId = ligneDeVenteId;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Long getId() {
		return ligneDeVenteId;
	}

	public void setId(Long id) {
		this.ligneDeVenteId = id;
	}

}
