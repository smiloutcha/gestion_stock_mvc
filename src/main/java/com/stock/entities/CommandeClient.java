package com.stock.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CommandeClient implements Serializable {

	@Id
	@GeneratedValue
	private Long commandeClientId;

	private String code;

	@Temporal(TemporalType.TIMESTAMP)
	private Date commandeDate;

	@ManyToOne
	@JoinColumn(name = "clientId")
	private Client client;
	
	@OneToMany(mappedBy ="commandeClient")
	private List<LigneCommandeClient> ligneCommandeClients;

	public Long getId() {
		return commandeClientId;
	}

	public void setId(Long id) {
		this.commandeClientId = id;
	}

	public Long getCommandeClientId() {
		return commandeClientId;
	}

	public void setCommandeClientId(Long commandeClientId) {
		this.commandeClientId = commandeClientId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCommandeDate() {
		return commandeDate;
	}

	public void setCommandeDate(Date commandeDate) {
		this.commandeDate = commandeDate;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<LigneCommandeClient> getLigneCommandeClients() {
		return ligneCommandeClients;
	}

	public void setLigneCommandeClients(List<LigneCommandeClient> ligneCommandeClients) {
		this.ligneCommandeClients = ligneCommandeClients;
	}

}
