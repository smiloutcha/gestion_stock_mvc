package com.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommandeFournisseur implements Serializable {

	@Id
	@GeneratedValue
	private Long ligneCommandeFournisseurId;

	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;

	@ManyToOne
	@JoinColumn(name = "commandeFournisseurId")
	private CommandeFournisseur commandeFournisseur;

	public Long getId() {
		return ligneCommandeFournisseurId;
	}

	public void setId(Long id) {
		this.ligneCommandeFournisseurId = id;
	}

	public Long getLigneCommandeFournisseurId() {
		return ligneCommandeFournisseurId;
	}

	public void setLigneCommandeFournisseurId(Long ligneCommandeFournisseurId) {
		this.ligneCommandeFournisseurId = ligneCommandeFournisseurId;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}

}
